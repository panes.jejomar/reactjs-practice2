import React, { useState} from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom'

import './App.css';
import Login from './pages/LogIn';
import Logout from './pages/Logout';
import Register from './pages/Register';
import AppNavbar from './components/Navbar';
import Home from './pages/Home'
import Error from './pages/Error'
import Product from './pages/Product'
import ProductView from "./pages/ProductView";
import Cart from './pages/Cart'

import { UserProvider } from "./UserContext";



function App() {

  const [user, setUser] = useState({
    accessToken : localStorage.getItem('accessToken'),
    email : localStorage.getItem("email"),
    
    isAdmin: localStorage.getItem("isAdmin")=== 'true'
  });
  
  const unsetUser = () =>{
    localStorage.clear()
  }

  return (

      <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
          <AppNavbar />
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/register" element={<Register/>} />
            <Route path="/logout" element={<Logout/>} />
            <Route path="/*" element={<Error/>}/>
            <Route path="/product" element={<Product/>} />
            <Route path="/product/:productId" element={<ProductView/>} />
            <Route path="/products/cart" element={<Cart/>}/>
          </Routes>
      </Router>
      </UserProvider>
  );
}

export default App;
