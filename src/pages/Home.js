import React from "react";
import { Button, Row, Col } from 'react-bootstrap';
import AppCarousel from "../components/AppCarousel";

export default function Home() {
    return (
        <div>
            <Row>
                <Col lg={12}>
                    <AppCarousel />
                </Col>
                <Col lg={4} className='p-4 m-auto'>
                    <div>
                        <h1>Welcome!</h1>
                    </div>
                
                </Col>
            </Row>
        </div>
    )
}