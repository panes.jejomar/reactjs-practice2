import React, { useEffect,useState, useContext } from "react";
import { Button, Form, Card, Row, Col } from 'react-bootstrap';
import swal from "sweetalert2";
import { Navigate,useNavigate } from "react-router-dom";
import UserContext from "../UserContext";



export default function Register() {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);
  const { user } = useContext(UserContext);

  useEffect(() => {
    if (
      (email !== "" &&
      password !== "" &&
      password2 !== "")
      && (password ===password2)
    ) {
      setIsActive(true)
    } else{
      setIsActive(false)
    }
  }, [email, password, password2]);

  function registerUser(e) {
    e.preventDefault();
    setEmail("");
    setPassword("");
    setPassword2("");

    fetch("https://ancient-ridge-75712.herokuapp.com/users/register", {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    }).then((result) => result.json())
    .then((data) => {
      console.log(data);
      if (data === true) {
        swal.fire({
          title: "Well done",
          icon: "success",
          text: "You have successfully registered"
        });
        navigate('/users/login')
      } else{
        swal.fire({
          title: "Failed",
          icon: "error",
          text: `${data.message}`
        });
      }
    });
  }

    return user.accessToken !== null ? (
      <Navigate to = 'users/login' />
    ) : (
      <div className="App">
              
              <Row>
                <Col lg={4} className='p-4 m-auto'>
                  <Card className='p-4'>
                    <Form onSubmit={(e) => registerUser(e)}>
                    <h1>Register</h1>
                      <Form.Group>
                        <Form.Label>Email Address: </Form.Label>
                        <Form.Control type='email' placeholder='insert email' value={email} onChange={ (e) => setEmail(e.target.value)} />
                        
                      </Form.Group>
            
                      <Form.Group>
                        <Form.Label>Password: </Form.Label>
                        <Form.Control type='password' placeholder='input your passowrd' value={password} onChange={ (e) => setPassword(e.target.value)} />
                      </Form.Group>

                      <Form.Group>
                        <Form.Label>Password: </Form.Label>
                        <Form.Control type='password' placeholder='confirm passowrd' value={password2} onChange={ (e) => setPassword2(e.target.value)} />
                      </Form.Group>
          
                      {isActive ? (
                      <Button variant="primary" type="submit" className="mt-1">
                        Submit
                      </Button>
                    ) : (
                      <Button
                        variant="secondary"
                        type="submit"
                        className="mt-1"
                        disabled
                      >
                        Submit
                      </Button>
                    )}
                    </Form>
                  </Card>
                </Col>
              </Row>

            </div>
      )
}